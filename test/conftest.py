"""Shared pytest fixtures."""

# SPDX-FileCopyrightText: 2018-2020 Christopher Kerr
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import hypothesis
import numpy as np
import pyopencl as cl
import pytest


# Workaround for issue #42 - flaky test timing due to OpenCL compilation time
hypothesis.settings.register_profile('long-deadline', deadline=10000)
hypothesis.settings.load_profile('long-deadline')


@pytest.fixture(scope='session')
def cl_context():
    """Get a pyopencl context."""
    return cl.create_some_context()


@pytest.fixture(scope='module')
def cl_queue(cl_context):
    """Get a pyopencl Queue."""
    with cl.CommandQueue(cl_context) as queue:
        yield queue


@pytest.fixture(scope='module')
def rng():
    # N.B. numpy.random.randint(a, b) excludes b,
    # random.randint(a, b) includes b as a possible value
    return np.random.RandomState(1234)
