# SPDX-FileCopyrightText: 2019-2020 Christopher Kerr
#
# SPDX-License-Identifier: CC0-1.0
#
# Dockerfile for the 'base image'
# This installs all the required dependencies in a docker image so that they
# do not have to be installed every time the tests are run
FROM debian:10
RUN apt-get update --quiet && \
    apt-get install --assume-yes --quiet --no-install-recommends python3-pyopencl pocl-opencl-icd gfortran python3-dev && \
    apt-get install --assume-yes --quiet --no-install-recommends git python3-scipy tox && \
    apt-get clean